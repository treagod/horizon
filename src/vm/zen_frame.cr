module Zen
  module Vm
    class Frame
      @variables : Hash(Int32, Int32)

      def initialize
        @variables = Hash(Int32, Int32).new
      end

      def set_variable(var_no : Int32, var_val : Int32) : Nil
        @variables[var_no] = var_val
      end

      def get_variable(var_no : Int32) : Int32
        return @variables[var_no]
      end
    end
  end
end
