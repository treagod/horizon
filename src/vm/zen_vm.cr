require "./zen_frame"

module Zen
  module Vm
    enum Instruction
      Halt = 0, Push, Add, Sub, Mul, Div, And, Or, Store, Load, IsGt, IsLt, Je

      def self.to_instr(instr : Int32)
        case instr
        when 0
          return Halt
        when 1
          return Push
        when 2
          return Add
        when 3
          return Sub
        when 4
          return Mul
        when 5
          return Div
        when 6
          return And
        when 7
          return Or
        when 8
          return Store
        when 9
          return Load
        when 10
          return IsGt
        when 11
          return IsLt
        when 12
          return Je
        else
          puts "Invalid instruction #{instr}. Exiting now."
          exit(1)
        end
      end
    end
    class ZenVm
      @stack : StaticArray(Int32, 1024)
      @instructions : Array(Int32)
      @current_frame : Frame
      def initialize(@instructions : Array(Int32))
        @stack = StaticArray(Int32, 1024).new(0)
        @current_frame = Frame.new
        @sp = 0
        @ip = 0
        @running = true
      end

      def running? : Bool
        return @running
      end

      def print_stack : Nil
        if @sp > 0
          (0..(@sp-1)).each do |i|
            puts @stack[i]
          end
        end
      end

      def run : Nil
        while running?

          define_ops({
            "+" => Instruction::Add,
            "-" => Instruction::Sub,
            "*" => Instruction::Mul,
            "/" => Instruction::Div,
            })
        end
      end

      macro define_ops(operations)
        instr = next_instruction
        case instr
        when Instruction::Halt
          puts "Stop running."
          @running = false
        when Instruction::Push
          @stack[@sp] = @instructions[@ip]
          inc_ip
          inc_sp
        {% for op, token in operations %}
          when {{token}}
            val = @stack[@sp-1]
            dec_sp
            @stack[@sp-1] = @stack[@sp-1] {{op.id}} val
        {% end %}
        when Instruction::And
          val = @stack[@sp-1]
          dec_sp
          @stack[@sp-1] = to_int(to_bool(@stack[@sp-1]) && to_bool(val))
        when Instruction::Or
          val = pop
          @stack[@sp-1] = to_int(to_bool(@stack[@sp-1]) || to_bool(val))
        when Instruction::IsLt
          val = @stack[@sp-1]
          dec_sp
          @stack[@sp-1] = (@stack[@sp-1] < val) ? 1 : 0
        when Instruction::IsGt
          val = @stack[@sp-1]
          dec_sp
          @stack[@sp-1] = (@stack[@sp-1] > val) ? 1 : 0
        when Instruction::Load
          @stack[@sp] = @current_frame.get_variable @instructions[@ip]
          inc_ip
          inc_sp
        when Instruction::Store
          var = @instructions[@ip]
          inc_ip
          val = @stack[@sp-1]
          dec_sp
          @current_frame.set_variable var, val
        when Instruction::Je
          val = @stack[@sp-1]
          addr = @instructions[@ip]
          dec_sp
          inc_ip
          if (to_bool(val) == true)
            @ip = addr
          end
        else
          puts "Something went wrong. Exiting. now"
          exit(1)
        end
      end

      private def pop() : Int32
        val = @stack[@sp-1]
        dec_sp
        return val
      end

      private def inc_ip
        @ip += 1
      end

      private def inc_sp
        @sp += 1
      end

      private def dec_ip
        @ip -= 1
      end

      private def dec_sp
        @sp -= 1
      end

      private def next_instruction : Instruction
        instr = Instruction.to_instr(@instructions[@ip])
        inc_ip
        return instr
      end

      private def to_int(b : Bool)
        return b ? 1 : 0
      end

      private def to_bool(i : Int32)
        return i != 0
      end
    end
  end
end
