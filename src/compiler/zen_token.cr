module Zen
  module Compiler
    enum TokenType
      # General
      Eof, Error, Integer, Float, String, Char, Boolean, Variable,

      # Keywords
      Def, Super, Default, If, Else, Elsif, True, False,
      Switch, Return, Break, Continue, While, With, In, Class,
      Struct, Private, Public, Include, Require, End, Do, Case, Null,
      Unless, Interface, When,

      # Operators
      Mul, Div, Mod, Bit_And, Bit_Or, Bit_Xor, And, Or, Not, Add, Sub,
      Bit_Not, Range_Excluded, Range_Included, Less, Less_Eq, Greater,
      Greater_Eq, Eq, Not_Eq, Ternary, Assign, Div_Assign, Mul_Assign,
      Add_Assign, Sub_Assign, Or_Assign, And_Assign, Arrow,

      # Punctuators
      Newline, LParen, RParen, LBracket, RBracket, LBrace, RBrace,
      Colon, Comma, Dot
    end

    OPERATORS = {"+" => TokenType::Add, "-" => TokenType::Sub, "*" => TokenType::Mul, "/" => TokenType::Div,
      "+=" => TokenType::Add_Assign, "-=" => TokenType::Sub_Assign, "/=" => TokenType::Div_Assign,
      "*=" => TokenType::Mul_Assign, "=" => TokenType::Assign, "&" => TokenType::Bit_And,
      "&&" => TokenType::And, "|" => TokenType::Bit_Or, "||" => TokenType::Or,
      "&&=" => TokenType::And_Assign, "||=" => TokenType::Or_Assign, "<" => TokenType::Less,
      "<=" => TokenType::Less_Eq, ">=" => TokenType::Greater_Eq, ">" => TokenType::Greater,
      "?" => TokenType::Ternary, ":" => TokenType::Colon, "." => TokenType::Dot,
      "(" => TokenType::LParen, ")" => TokenType::RParen, "[" => TokenType::LBracket,
      "]" => TokenType::RBracket, "{" => TokenType::LBrace, "}" => TokenType::RBrace,
      "==" => TokenType::Eq, "!=" => TokenType::Not_Eq, "->" => TokenType::Arrow, "!" => TokenType::Not}

    KEYWORDS = {"do" => TokenType::Do, "if" => TokenType::If, "else" => TokenType::Else,
      "elsif" => TokenType::Elsif, "with" => TokenType::With, "while" => TokenType::While,
      "class" => TokenType::Class, "def" => TokenType::Def, "end" => TokenType::End,
      "return" => TokenType::Return, "continue" => TokenType::Continue, "break" => TokenType::Break,
      "case" => TokenType::Case, "when" => TokenType::Case, "default" => TokenType::Default,
      "interface" => TokenType::Interface, "true" => TokenType::True, "false" => TokenType::False,
      "public" => TokenType::Public, "private" => TokenType::Private, "require" => TokenType::Require,
      "include" => TokenType::Include, "unless" => TokenType::Unless, "in" => TokenType::In}

    class Token
      getter :type, :lexeme, :line, :col
      def initialize(@type : TokenType, @pos : Int32, @line : Int32,
                    @col : Int32, @lexeme : String)
      end

      def to_s(io)
        io << "<Token type='" << @type << "' pos='" << @line << ":" << @col << "' value='" << @lexeme << "'>"
      end
    end # Token
  end # Compiler
end # Zen
