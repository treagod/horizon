require "./zen_lexer"
require "./zen_token"
require "./zen_ast"

module Zen
  module Compiler
    enum Precedence
      Lowest = 0, Comma, Ternary, Or, And, Bit_Or, Bit_Xor, Bit_And, Equals,
      LessGreater, Shift, Sum, Product, Prefix, Call

      def self.lookup(token : Token) : Precedence
        case token.type
        when TokenType::Add, TokenType::Sub
          return Sum
        when TokenType::Mul, TokenType::Div
          return Product
        when TokenType::Eq, TokenType::Not_Eq
          return Equals
        when TokenType::Or
          return Or
        when TokenType::And
          return And
        when TokenType::Bit_And
          return Bit_And
        when TokenType::Bit_Or
          return Bit_Or
        when TokenType::Less, TokenType::Less_Eq, TokenType::Greater, TokenType::Greater_Eq
          return LessGreater
        when TokenType::Eq, TokenType::Not_Eq
          return Equals
        else
          return Lowest
        end
      end
    end
    class Parser
      @current_token : Token
      @lookahead_token : Token
      @parse_prefix : Hash(TokenType, Proc(Expression))
      @parse_infix : Hash(TokenType, Proc(Expression, Expression))

      def initialize(@lexer : Lexer)
        @current_token = @lexer.next_token
        @lookahead_token = @lexer.next_token
        @parse_prefix = Hash(TokenType, Proc(Expression)).new
        @parse_infix = Hash(TokenType, Proc(Expression, Expression)).new
        init_prefix_funcs
        init_infix_funcs
      end

      def parse : TranslationUnit
        tu = TranslationUnit.new

        while @current_token.type != TokenType::Eof
          skip_newline

          tu.add next_stmt

          skip_newline
        end

        return tu
      end

      private def next_stmt : Statement
        if @current_token.type == TokenType::Variable && (@lookahead_token.type == TokenType::Comma ||
          @lookahead_token.type == TokenType::Assign || @lookahead_token.type == TokenType::Add_Assign ||
          @lookahead_token.type == TokenType::Sub_Assign || @lookahead_token.type == TokenType::Mul_Assign ||
          @lookahead_token.type == TokenType::Div_Assign)

          if @lookahead_token.type == TokenType::Assign
            return parse_assign
          #elsif @lookahead_token.type == TokenType::Comma
          #  return parse_list_assign
          else
            typ = @lookahead_token.type
            return parse_operator_assign typ
          end
        end

        if @current_token.type == TokenType::If
          return parse_if
        end

        if @current_token.type == TokenType::Unless
          return parse_unless
        end

        return ExpressionStatement.new parse_expr(Precedence::Lowest)
      end

      private def parse_if : Statement
        exit(1)
      end

      private def parse_unless : Statement
        exit(1)
      end

      private def parse_assign : Statement
        var = @current_token
        consume(TokenType::Variable)
        var_literal  = VariableLiteral.new var
        token= @current_token
        consume(TokenType::Assign)
        expr = parse_expr Precedence::Lowest
        return AssignStatement.new token, var_literal, expr
      end

      private def parse_operator_assign(typ : TokenType) : Statement
        var = @current_token
        consume(TokenType::Variable)
        var_literal  = VariableLiteral.new var
        token= @current_token
        consume(typ)
        expr = parse_expr Precedence::Lowest
        return OperatorAssignStatement.new token, var_literal, expr
      end

      private def parse_expr(prec : Precedence) : Expression
        if @parse_prefix.has_key? @current_token.type
          expr = @parse_prefix[@current_token.type].call
        else
          puts "Unexpected prefix token #{@current_token.type} at #{@current_token.line}:#{@current_token.col}."
          exit(1)
        end

        while (@current_token.type != TokenType::Eof && prec < Precedence.lookup(@current_token))
          if @parse_infix.has_key? @current_token.type
            expr = @parse_infix[@current_token.type].call(expr)
          else
            puts "Unexpected token #{@current_token.type} at #{@current_token.line}:#{@current_token.col}."
            exit(1)
          end
        end

        return expr
      end

      private def skip_newline : Nil
        while @current_token.type == TokenType::Newline
          consume TokenType::Newline
        end
      end

      macro define_literal(literals)
        {% for literal, typ in literals %}
          @parse_prefix[{{typ}}] = Proc(Expression).new do
            {% if typ == TokenType::True || typ == TokenType::False %}
              literal = BooleanLiteral.new @current_token
            {% else %}
              literal = {{literal}}Literal.new @current_token
            {% end %}
            consume {{typ}}
            return literal
          end
        {% end %}
      end

      private def init_prefix_funcs
        define_literal({
          Integer => TokenType::Integer,
          Boolean => TokenType::True,
          #Booleanf => TokenType::False
          Variable => TokenType::Variable,
        })
        @parse_prefix[TokenType::False] = Proc(Expression).new do
          bool_literal = BooleanLiteral.new @current_token
          consume TokenType::False
          return bool_literal
        end
        @parse_prefix[TokenType::Not] = Proc(Expression).new do
          token = @current_token
          consume TokenType::Not
          return PrefixExpression.new(token, parse_expr(Precedence::Prefix))
        end
        @parse_prefix[TokenType::Sub] = Proc(Expression).new do
          token = @current_token
          consume TokenType::Sub
          return PrefixExpression.new(token, parse_expr(Precedence::Prefix))
        end
        @parse_prefix[TokenType::Add] = Proc(Expression).new do
          token = @current_token
          consume TokenType::Add
          return PrefixExpression.new(token, parse_expr(Precedence::Prefix))
        end
        @parse_prefix[TokenType::LParen] = Proc(Expression).new do
          consume(TokenType::LParen)
          expr = parse_expr Precedence::Lowest
          consume(TokenType::RParen)
          return expr
        end
      end

      macro define_infix_op(types)
        {% for type, index in types %}
          @parse_infix[{{type}}] = Proc(Expression, Expression).new do |left_expr|
            token = @current_token
            consume({{type}})

            right_expr = parse_expr(Precedence.lookup(token))
            return InfixExpression.new(token, left_expr, right_expr)
          end
        {% end %}
      end

      private def init_infix_funcs
        define_infix_op [TokenType::Add, TokenType::Mul, TokenType::Div, TokenType::Sub, TokenType::Eq,
          TokenType::Not_Eq, TokenType::Less, TokenType::Less_Eq, TokenType::Greater, TokenType::Greater_Eq,
          TokenType::And, TokenType::Or, TokenType::Bit_And, TokenType::Bit_Or
        ]
      end

      private def consume(typ : TokenType) : Nil
        if @current_token.type == typ
          @current_token = @lookahead_token
          @lookahead_token = @lexer.next_token
        else
          puts "Expected #{typ}, got #{@current_token.type}"
          exit(1)
        end
      end
    end
  end
end
