module Zen
  module Compiler
    abstract class Statement
      abstract def to_s(io)
    end

    abstract class Expression
      abstract def token_lexeme : String
      abstract def to_s(io)
    end

    class TranslationUnit
      getter :stmts
      @stmts : Array(Statement)

      def initialize
        @stmts = Array(Statement).new
      end

      def add(stmt : Statement)
        @stmts << stmt
      end
    end

    class AssignStatement < Statement
      def initialize(@token : Token, @var : VariableLiteral, @expr : Expression)
      end

      def token_lexeme
        @token.lexeme
      end

      def to_s(io)
        io << @var << token_lexeme << @expr
      end
    end

    class OperatorAssignStatement < AssignStatement
      def to_s(io)
        io << @var << "=" << @var << op << @expr
      end

      private def op : String
        return "#{token_lexeme[0]}"
      end
    end

    class ExpressionStatement < Statement
      getter :expr

      def initialize(@expr : Expression)
      end

      def to_s(io)
      end
    end

    class InfixExpression < Expression
      getter :token, :left_expr, :right_expr
      def initialize(@token : Token, @left_expr : Expression, @right_expr : Expression)
      end

      def token_lexeme
        token.lexeme
      end

      def to_s(io)
        io << "(" << left_expr << token_lexeme << right_expr << ")"
      end
    end

    class PrefixExpression < Expression
      getter :token, :expr
      def initialize(@token : Token, @expr : Expression)
      end

      def token_lexeme
        token.lexeme
      end

      def to_s(io)
        io << token_lexeme << "(" << expr << ")"
      end
    end

    class IntegerLiteral < Expression
      getter  :token
      def initialize(@token : Token)
      end

      def token_lexeme
        token.lexeme
      end

      def to_s(io)
        io << token.lexeme
      end

      def value
        token_lexeme.to_i
      end
    end

    class BooleanLiteral < Expression
      getter  :token
      def initialize(@token : Token)
      end

      def token_lexeme
        token.lexeme
      end

      def value
        return token.lexeme == "true"
      end

      def to_s(io)
        io << token.lexeme
      end
    end

    class VariableLiteral < Expression
      # VHIT = Vom Hirn ins Terminal
      getter  :token, :type
      def initialize(@token : Token, @type : String = "void")
      end

      def token_lexeme
        token.lexeme
      end

      def to_s(io)
        io << token.lexeme
      end
    end
  end
end
