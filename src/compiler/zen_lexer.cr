module Zen
  module Compiler
    class Lexer
      @buffer : Iterator(Char)
      @current_char : Char
      @next_char : Char

      def initialize(source : String)
        @position = 0
        @line_no = 1
        @col_no = 1
        @buffer = source.each_char
        @current_char = get_next_char
        @next_char = get_next_char
      end

      def next_token() : Token
        while is_whitespace || is_single_comment
          skip_whitespace
          skip_comment
        end
        if @current_char == '\0'
          return next_token TokenType::Eof, "EOF"
        end

        if @current_char.number?
          return next_number
        end

        if @current_char.letter? || @current_char == '@'
          return next_var_or_key
        end

        if OPERATORS.has_key? "#{@current_char}"
            return next_operator
        end

        if @current_char == '\n'
          return next_token TokenType::Newline, "\\n"
        end

        if @current_char == ','
          return next_token TokenType::Comma, ","
        end

        invalid_char_sequence
      end

      private def get_next_char() : Char
        char = @buffer.next
        if (char.is_a?(Iterator::Stop))
          return '\0'
        else
          return char.as(Char)
        end
      end

      private def next_operator : Token
        pos, line, col = @position, @line_no, @col_no

        op = adjust_operator

        typ = get_operator_type op

        return Token.new(typ, pos, line, col, op)
      end

      private def adjust_operator : String
        case @current_char
        when '&'
          if @next_char == '&'
            return adjust_logic_operator
          elsif @next_char == '='
            return operator_assign
          end
        when '|'
          if @next_char == '|'
            return adjust_logic_operator
          elsif @next_char == '='
            return operator_assign
          end
        when '-'
          if @next_char == '='
            return operator_assign
          elsif @next_char == '>'
            op = "#{@current_char}#{@next_char}"
            advance; advance
            return op
          end
        when '+', '*',  '/', '<', '>', '!', '='
          if @next_char == '='
            return operator_assign
          end
        end
        op = "#{@current_char}"
        advance
        return op
      end

      private def adjust_logic_operator : String
        first_char = @current_char
        advance
        if @next_char == '='
          op = "#{first_char}#{@current_char}#{@next_char}"
          advance
        else
          op = "#{first_char}#{@current_char}"
        end
        advance
        return op
      end

      private def operator_assign : String
        op = "#{@current_char}#{@next_char}"
        advance; advance
        return op
      end

      private def next_number : Token
        is_float = false
        pos, line, col = @position, @line_no, @col_no
        str_builder = String::Builder.new
        while @current_char.number? || char_is_dot
          if char_is_dot
            is_float = true
            str_builder << @current_char
            advance
            while @current_char.number?
              str_builder << @current_char
              advance
            end
            break
          end
          str_builder << @current_char
          advance
        end

        token_type = is_float ? TokenType::Float : TokenType::Integer

        return Token.new token_type, pos, line, col, str_builder.to_s
      end

      private def next_var_or_key : Token
        pos, line, col = @position, @line_no, @col_no
        builder = String::Builder.new
        builder << @current_char
        advance

        while @current_char.letter? || @current_char.number? || @current_char == '_' ||
          @current_char == '?' ||  @current_char == '!'
          builder << @current_char
          advance
        end
        lexeme = builder.to_s

        type  = (KEYWORDS.has_key?(lexeme)) ? get_keyword_type(lexeme) :  TokenType::Variable

        return Token.new(type, pos, line, col, lexeme)
      end

      private def next_token(type : TokenType, lexeme : String) : Token
        pos, line, col = @position, @line_no, @col_no
        advance
        return Token.new(type, pos, line, col, lexeme)
      end

      private def advance : Nil
        if @current_char == '\n'
          @line_no += 1
          @col_no = 1
        else
          @col_no += 1
        end
        @current_char = @next_char
        @next_char = get_next_char
      end

      private def is_single_comment
        return @current_char == '#'
      end

      private def is_whitespace : Bool
        return @current_char == ' ' || @current_char == '\t'
      end

      private def get_operator_type(op : String) : TokenType
        if OPERATORS.has_key? op
          return OPERATORS[op]
        else
          puts "Invalid operator at #{@line_no}:#{@col_no}"
          exit(1)
        end
      end

      private def get_keyword_type(lexeme : String) : TokenType
        # create hash
        if KEYWORDS.has_key? lexeme
          return KEYWORDS[lexeme]
        else
          puts "Undefined keyword at #{@line_no}:#{@col_no}."
          exit(1)
        end
      end

      private  def invalid_char_sequence
        puts "Invalid char sequence '#{@current_char}' at #{@line_no}:#{@col_no}."
        exit(1)
      end

      private def skip_whitespace : Nil
        while is_whitespace
          advance
        end
      end

      private def skip_comment : Nil
        while @current_char != '\n' && @current_char != '\r'
          advance
        end
        advance
      end

      private def char_is_dot : Bool
        return @current_char == '.'
      end
    end # Lexer
  end # Compiler
end # Zen
