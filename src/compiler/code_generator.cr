module Zen
  module Compiler
    class CodeGenerator
      @code : Array(Int32)
      def initialize(@tu : TranslationUnit)
        @code = Array(Int32).new
      end

      def emit_code() : Array(Int32)

        @tu.stmts.each do |stmt|
          get_instructions stmt
        end

        @code << 0

        return @code
      end

      private def get_instructions(stmt : Statement) : Nil
        if stmt.is_a? ExpressionStatement
          expr = stmt.as(ExpressionStatement).expr

          get_expr_instruction expr
        end
      end

      private def get_expr_instruction(expr : Expression) : Nil
        if expr.is_a?(IntegerLiteral)
          int_lit = expr.as(IntegerLiteral)
          @code << 1 << int_lit.value
        elsif expr.is_a?(BooleanLiteral)
          bool_lit = expr.as(BooleanLiteral)
          @code << 1 << (bool_lit.value ? 1 : 0)
        elsif expr.is_a?(InfixExpression)
          inf_expr = expr.as(InfixExpression)
          instr_left = get_expr_instruction inf_expr.left_expr
          instr_right = get_expr_instruction inf_expr.right_expr

          @code << operation(inf_expr.token_lexeme)
        else
          @code << 0
        end
      end


      private def operation(op : String) : Int32
        case op
        when "+"
          return 2
        when "-"
          return 3
        when "*"
          return 4
        when "/"
          return 5
        when "&&"
          return 6
        when "||"
          return 7
        else
          puts "Unknown operation. Exiting now."
          exit(1)
        end
      end
    end
  end
end
