require "./compiler/*"
require "./vm/*"
require "option_parser"

module Zen
  interactive = false
  destination = "World"
  source_path = [] of String

  OptionParser.parse! do |parser|
    parser.banner = "Usage: zen filename [arguments]"
    parser.on("-i", "--interactive", "Start REPL") { interactive = true }
    # parser.on("-t NAME", "--to=NAME", "Specifies the name to salute") { |name| destination = name }
    parser.on("-h", "--help", "Show this help") { puts parser }
    parser.unknown_args() { |arg| source_path.concat arg }
  end

  unless interactive
    if source_path.size > 1
      puts "Specify one file to compile."
    elsif source_path.size == 0
      puts "Specify a file to compile."
    else
      file = File.read source_path[0]
      lexer = Compiler::Lexer.new file
      parser = Compiler::Parser.new lexer
      tu = parser.parse
      code_gen = Compiler::CodeGenerator.new(tu)
      code = code_gen.emit_code

      puts code
      vm = Vm::ZenVm.new([
        1, 32,
        1, 64,
        11,

        12, 9,

        1, 42,
        1, 1337,

        0
        ])
      vm.run
      vm.print_stack
    end
  else
    exit(1)
  end
end
